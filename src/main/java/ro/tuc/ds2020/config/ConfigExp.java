package ro.tuc.ds2020.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.support.RemoteExporter;
import ro.tuc.ds2020.services.SimulatorSensorService;

@Configuration
public class ConfigExp{

    private final SimulatorSensorService s;

    public ConfigExp(SimulatorSensorService s) {
        this.s = s;
    }

    @Bean(name = "/measurement_export_value")
    RemoteExporter bookingService() {
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(new ObjectRemote(s));
        exporter.setServiceInterface( IObjectRemote.class );
        return exporter;
    }
}