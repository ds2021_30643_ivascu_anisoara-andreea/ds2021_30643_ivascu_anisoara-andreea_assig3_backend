package ro.tuc.ds2020.config;

import ro.tuc.ds2020.dtos.SimulatorSensorDTO;
import ro.tuc.ds2020.services.SimulatorSensorService;

import java.util.Date;
import java.util.List;
import java.util.UUID;


public class ObjectRemote implements IObjectRemote {

    private final SimulatorSensorService simulatorSensorService;

    public ObjectRemote(SimulatorSensorService simulatorSensorService) {
        this.simulatorSensorService = simulatorSensorService;
    }

    @Override
    public String viewConnection(String connection) {

        connection="Connection made!!";
        return connection;
    }
   @Override
    public double[][] dates(UUID id,Date date, int days) {



        List<SimulatorSensorDTO> listOfValues = simulatorSensorService.findSimulatorSensor();
       // System.out.println(monitoredValueDTOList.get(0).getMeasurement_value());

       int startHour=0;
       int endHour=startHour-1;

        date.setHours(startHour);

        date.setHours(date.getHours() +endHour);
        Date newDate = new Date(date.getTime());
        Date dataEnd = new Date(newDate.getTime());
        dataEnd.setDate(dataEnd.getDate() - days);
      /*  for(int i=0;i<monitoredValueDTOList.size();i++)
            System.out.println(monitoredValueDTOList.get(i).getMeasurement_value());*/

        double[][] matrix = new double[days][24];
        int nrOfDays = 0;
       int eHour=0;
        while( dataEnd.getTime()<newDate.getTime()){
            int sHour=23;

          while(sHour>=eHour) {
                for( int k=0;k<listOfValues.size();k++){
                   // System.out.println(m.getTimestamp()+"         "+newDate);
                    if((listOfValues.get(k).getTimestamp().getYear() != newDate.getYear() || listOfValues.get(k).getTimestamp().getMonth() != newDate.getMonth() ||
                            listOfValues.get(k).getTimestamp().getDay() != newDate.getDay() || listOfValues.get(k).getTimestamp().getHours() != newDate.getHours()))
                    {
                        matrix[nrOfDays][sHour] += 0;
                    }
                    else
                        matrix[nrOfDays][sHour] += listOfValues.get(k).getMeasurement_value();
                   // System.out.println("ziua "+zile+"ora "+j+":"+a[zile][j]);
                }
                newDate.setHours(newDate.getHours() +endHour);
              sHour--;
            }
            nrOfDays++;
        }

        return matrix;
    }

}
