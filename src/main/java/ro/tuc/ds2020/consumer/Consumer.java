package ro.tuc.ds2020.consumer;
import org.apache.tomcat.jni.Time;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.config.MQConfig;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.SensorDetailsDTO;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.entities.SimulatorSensor;
import ro.tuc.ds2020.repositories.SensorRepository;
import ro.tuc.ds2020.repositories.SimulatorSensorRepository;
import ro.tuc.ds2020.services.ClientService;
import ro.tuc.ds2020.services.SensorService;
import ro.tuc.ds2020.services.SimulatorSensorService;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Component
public class Consumer {

    @Autowired
    private RabbitTemplate templateRabbit;

   @Autowired
    private SimulatorSensorRepository simulatorSensorRepository;

    @Autowired
    private SensorRepository sensorRepository;

    @Autowired
    private SensorService sensorService;



   private int diffT12=10;


    int ok=0;



    @RabbitListener(queues = MQConfig.QUEUE)
    public void listener(SimulatorSensor simulatorSensor) throws IllegalAccessException, NoSuchFieldException {
        System.out.println(simulatorSensor);
        simulatorSensorRepository.save(simulatorSensor);

        //String s=verifyPeak();
        //System.out.println("message for value:  "+s);


       // List<SimulatorSensor> l = simulatorSensorRepository.findAll();
        //UUID sensorID=l.get(0).getId();
       // SensorDetailsDTO sensor=sensorService.findSensorById(sensorID);
       // double maximumValue=sensor.getMaximumValue();
       // System.out.println(maximumValue);
       /*for(int i=0;i<l.size();i++) {
            if (getPeak(l.get(i + 1).getMeasurementValue() , l.get(i).getMeasurementValue())<maximumValue)
            {
                System.out.println("ok");
            }
            else
            {
            System.out.println("depaseste!!");
            }
        }*/



        System.out.println();
       /* if (l.size() > 2) {
            for (int i = 0; i < l.size(); i++) {
                double j = l.get(i + 1).getMeasurementValue();
                System.out.println(j);
                // System.out.println(l.get(i + 1).getMeasurementValue() - l.get(i).getMeasurementValue());
            }
        }*/
    }




        public double getPeak(double measurementValT1,double measurementValT2 )
        {
           double peak=(measurementValT2-measurementValT1)/diffT12;
           return peak;

        }

    public String verifyPeak( )
    {
        List<SimulatorSensor> l = simulatorSensorRepository.findAll();
        UUID sensorID=l.get(0).getSensor_id();
         SensorDetailsDTO sensor=sensorService.findSensorById(sensorID);
         double maximumValue=sensor.getMaximumValue();
         String s="";
         System.out.println(maximumValue);
         if(l.size()>2) {
             for (int i = 0; i < l.size(); i++) {
                 if (getPeak(l.get(i + 1).getMeasurement_value(), l.get(i).getMeasurement_value()) < maximumValue) {
                     System.out.println("ok");
                     s = "ok";
                 } else {
                     System.out.println("depaseste!!");
                     s = "depaseste";

                 }
             }
         }
       return s;

    }


        //  List<SimulatorSensor> sensors= new ArrayList<SimulatorSensor>();
       // sensors.add(message);




           /*for (int i = 0; i < sensors.size(); i++) {
               System.out.println(sensors.get(sensors.size()));
           }*/

           /* if(sensors.get(i+1).getMeasurementValue()-sensors.get(i).getMeasurementValue()<0.5)
            { ok=0;
                //System.out.println((sensors.get(i+1).getMeasurementValue()+" "+sensors.get(i).getMeasurementValue()));
            }
            else {ok=1; }*/
                // System.out.println(i+" "+sensors.get(i));
                //if ((sensors.get(i + 1).getMeasurementValue() - sensors.get(i).getMeasurementValue()) > 0.5)
              // double  b=sensors1.get(i+1).getMeasurementValue();
              //  double  a=  sensors.get(i).getMeasurementValue();
                   // System.out.println(sensors.get(i));
                //System.out.println(sensors.get(i+1));
                    //System.out.println(b);
            ///}
           // System.out.println(sensors.get(sensors.size()));

        /*if(ok==1) {
            System.out.println(message);
           // template.convertAndSend("/topic/simulator_sensor", message);
        }*/





}

