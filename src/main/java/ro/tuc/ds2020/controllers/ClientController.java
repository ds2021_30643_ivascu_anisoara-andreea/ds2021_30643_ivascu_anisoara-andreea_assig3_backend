package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.ClientDTO;
import ro.tuc.ds2020.dtos.ClientDetailsDTO;
import ro.tuc.ds2020.dtos.builders.ClientBuilder;
import ro.tuc.ds2020.entities.Client;
import ro.tuc.ds2020.services.ClientService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/client")
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping()
    public ResponseEntity<List<ClientDTO>> getClient() {
        List<ClientDTO> dtos = clientService.findClients();
        for (ClientDTO dto : dtos) {
            Link clientLink = linkTo(methodOn(ClientController.class)
                    .getClient(dto.getId())).withRel("clientDetails");
            dto.add(clientLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody ClientDetailsDTO clientDTO) {
        UUID clientID = clientService.insert(clientDTO);
        return new ResponseEntity<>(clientID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ClientDetailsDTO> getClient(@PathVariable("id") UUID clientId) {
        ClientDetailsDTO dto = clientService.findClientById(clientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    //TODO: UPDATE, DELETE per resource

    @PostMapping(value = "/delete/{id}")
    public ResponseEntity<HttpStatus> deleteClient(@PathVariable("id") UUID personId){
        clientService.deleteClientById(personId);
        return new ResponseEntity(new ClientDTO(),HttpStatus.OK);
    }

    @PostMapping(value = "/update")
    public ResponseEntity<UUID> updateClient(@Valid @RequestBody ClientDetailsDTO clientDTO) {
        UUID clientID = clientService.update(clientDTO);
        return new ResponseEntity<>(clientID, HttpStatus.CREATED);
    }


}
