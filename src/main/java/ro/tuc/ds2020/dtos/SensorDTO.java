package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class SensorDTO extends RepresentationModel<SensorDTO> {
    private UUID id;
    private String description;
    private float maximumValue;


    public SensorDTO() {
    }

    public SensorDTO(UUID id, String description, float maximumValue) {
        this.id = id;
        this.description = description;
        this.maximumValue = maximumValue;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(float maximumValue) {
        this.maximumValue = maximumValue;
    }



}
