package ro.tuc.ds2020.dtos;

import java.util.Date;
import java.util.UUID;

public class SimulatorSensorDetailsDTO {
    private UUID sensor_id;
    private Date timestamp;
    private double measurement_value;

    public SimulatorSensorDetailsDTO() {

    }

    public SimulatorSensorDetailsDTO(UUID sensor_id, Date timestamp, double measurement_value) {
        this.sensor_id = sensor_id;
        this.timestamp = timestamp;
        this.measurement_value = measurement_value;
    }

    public SimulatorSensorDetailsDTO( Date timestamp, double measurement_value) {

        this.timestamp = timestamp;
        this.measurement_value = measurement_value;
    }

    public UUID getSensor_id() {
        return sensor_id;
    }

    public void setSensor_id(UUID sensor_id) {
        this.sensor_id = sensor_id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public double getMeasurement_value() {
        return measurement_value;
    }

    public void setMeasurement_value(double measurement_value) {
        this.measurement_value = measurement_value;
    }
}
