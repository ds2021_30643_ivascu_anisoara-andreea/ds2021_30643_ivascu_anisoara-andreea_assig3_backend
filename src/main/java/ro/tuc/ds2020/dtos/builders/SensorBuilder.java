package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.Sensor;

public class SensorBuilder {

    private SensorBuilder() {
    }

    public static SensorDTO toSensorDTO(Sensor sensor) {
        return new SensorDTO(
                sensor.getId(),
                sensor.getDescription(),
                sensor.getMaximumValue()
        );
    }

    public static Sensor toEntity(SensorDetailsDTO sensorDetailsDTO) {
        return new Sensor(
                sensorDetailsDTO.getDescription(),
                sensorDetailsDTO.getMaximumValue()

        );
    }

    public static SensorDetailsDTO toSensorDetailsDTO(Sensor sensor) {
        return new  SensorDetailsDTO (
                sensor.getId(),
                sensor.getDescription(),
                sensor.getMaximumValue()
        );
    }
}
