package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.SensorDetailsDTO;
import ro.tuc.ds2020.dtos.SimulatorSensorDTO;
import ro.tuc.ds2020.dtos.SimulatorSensorDetailsDTO;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.entities.SimulatorSensor;

public class SimulatorSensorBuilder {

    private SimulatorSensorBuilder() {
    }

    public static SimulatorSensorDTO toSimulatorSensorDTO(SimulatorSensor simulatorSensor) {
        return new SimulatorSensorDTO(
                simulatorSensor.getSensor_id(),
                simulatorSensor.getTimestamp(),
                simulatorSensor.getMeasurement_value()
        );
    }

    public static SimulatorSensor toEntity(SimulatorSensorDetailsDTO simulatorSensorDetailsDTO) {
        return new SimulatorSensor(
                simulatorSensorDetailsDTO.getTimestamp(),
                simulatorSensorDetailsDTO.getMeasurement_value()

        );
    }

    public static SimulatorSensorDetailsDTO toSimulatorSensorDetailsDTO(SimulatorSensor simulatorSensor) {
        return new  SimulatorSensorDetailsDTO (
                simulatorSensor.getSensor_id(),
                simulatorSensor.getTimestamp(),
                simulatorSensor.getMeasurement_value()
        );
    }
}
