package ro.tuc.ds2020.entities;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
//@AllArgsConstructor
@ToString
@Entity
public class SimulatorSensor {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID sensor_id;
    private Date timestamp;
    private double measurement_value;

    public SimulatorSensor(Date timestamp, double measurement_value) {
        this.timestamp = timestamp;
        this.measurement_value = measurement_value;
    }

    public SimulatorSensor(UUID sensor_id, Date timestamp, double measurement_value) {
        this.sensor_id = sensor_id;
        this.timestamp = timestamp;
        this.measurement_value = measurement_value;
    }

    public UUID getSensor_id() {
        return sensor_id;
    }

    public void setSensor_id(UUID sensor_id) {
        this.sensor_id = sensor_id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public double getMeasurement_value() {
        return measurement_value;
    }

    public void setMeasurement_value(double measurement_value) {
        this.measurement_value = measurement_value;
    }
}