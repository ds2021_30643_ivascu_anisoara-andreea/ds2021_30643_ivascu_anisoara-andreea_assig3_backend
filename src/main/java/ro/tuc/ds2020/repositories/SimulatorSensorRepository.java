package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.entities.SimulatorSensor;

import java.util.UUID;

@Repository
public interface SimulatorSensorRepository extends JpaRepository<SimulatorSensor, UUID> {
}
