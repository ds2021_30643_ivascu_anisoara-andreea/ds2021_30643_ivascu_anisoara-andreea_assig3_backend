package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.entities.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    /**
     * Example: JPA generate Query by Field
     */
    List<User> findByUsername(String username);

    @Query(value = "SELECT u " +
            "FROM User u " +
            "WHERE u.id = :id")
    Optional<User> findWithId(@Param("id") Integer id);

    /**
     * Example: Write Custom Query
     */
    @Query(value = "SELECT u " +
            "FROM User u " +
            "WHERE u.username = :username ")
    Optional<User> findSeniorsByUsername(@Param("username") String username);


}
