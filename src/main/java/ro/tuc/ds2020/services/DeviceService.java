package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;

import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceDetailsDTO;

import ro.tuc.ds2020.dtos.builders.DeviceBuilder;

import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.repositories.DeviceRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DeviceService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceService.class);
    private final DeviceRepository deviceRepository;

    @Autowired
    public DeviceService(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    public List<DeviceDTO> findDevices() {
        List<Device> deviceList = deviceRepository.findAll();
        return deviceList.stream()
                .map(DeviceBuilder::toDeviceDTO)
                .collect(Collectors.toList());
    }

    public DeviceDetailsDTO  findDeviceById(UUID id) {
        Optional<Device> prosumerOptional = deviceRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Device with id"+ id +" was not found in db");
            throw new ResourceNotFoundException(Device.class.getSimpleName() + " with id: " + id);
        }
        return DeviceBuilder.toDeviceDetailsDTO(prosumerOptional.get());
    }

    public UUID insert(DeviceDetailsDTO deviceDTO) {
        Device device = DeviceBuilder.toEntity(deviceDTO);
        device = deviceRepository.save(device);
        LOGGER.debug("Device with id"+device.getId() +" was inserted in db");
        return device.getId();
    }

    /*public void delete(UUID deviceId)
    {
        this.deviceRepository.deleteById(deviceId);
    }*/

    public void deleteDeviceById(UUID id) {
        deviceRepository.deleteById(id);
        Optional<Device> toSave = deviceRepository.findById(id);
        if(toSave.isPresent()){
            LOGGER.debug("Device with id {} was not deleted", id);
        }

    }
    /*public UUID update(UUID id, DeviceDetailsDTO deviceDTO)
    {

        Device device = DeviceBuilder.toEntity(deviceDTO);
        device.setId(id);
        device = deviceRepository.save(device);
        LOGGER.debug("Device with id" +device.getId() + "was inserted in db");
        return device.getId();
    }*/


    public UUID update(DeviceDetailsDTO deviceDTO){
        Device device = DeviceBuilder.toEntity(deviceDTO);
        device.setId(deviceDTO.getId());
        Optional<Device> toSave = deviceRepository.findById(device.getId());
        if(!toSave.isPresent()){
            LOGGER.debug("Person with id {} is not in the db", device.getId());
            throw new ResourceNotFoundException(Device.class.getSimpleName() + " with id: " + device.getId());
        }
        deviceRepository.save(device);
        LOGGER.debug("Person with id {} was updated", device.getId());

        return device.getId();
    }


    public DeviceDTO findDevice(DeviceDTO deviceDTO)
    {
        List<Device> users = deviceRepository.findAll();

        for(Device d: users)
        {
            //   if((d.getUsername().equals(personDTO.getUsername())) && (d.getPassword().equals(personDTO.getPassword())))
            // {
            //   return DeviceBuilder.toDeviceDTO(p);
            // }
        }
        throw new ResourceNotFoundException(" not found!");
    }


}
