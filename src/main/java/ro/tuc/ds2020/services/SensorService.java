package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.ClientDetailsDTO;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.SensorDetailsDTO;
import ro.tuc.ds2020.dtos.builders.ClientBuilder;
import ro.tuc.ds2020.dtos.builders.SensorBuilder;
import ro.tuc.ds2020.entities.Client;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.SensorRepository;


import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SensorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SensorService.class);
    private final SensorRepository sensorRepository;

    @Autowired
    public SensorService(SensorRepository sensorRepository) {
        this.sensorRepository = sensorRepository;
    }

    public List<SensorDTO> findSensors() {
        List<Sensor> sensorList = sensorRepository.findAll();
        return sensorList.stream()
                .map(SensorBuilder::toSensorDTO)
                .collect(Collectors.toList());
    }

    public SensorDetailsDTO findSensorById(UUID id) {
        Optional<Sensor> prosumerOptional = sensorRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Sensor with id"+ id +" was not found in db");
            throw new ResourceNotFoundException(Sensor.class.getSimpleName() + " with id: " + id);
        }
        return SensorBuilder.toSensorDetailsDTO(prosumerOptional.get());
    }

    public UUID insert(SensorDetailsDTO sensorDTO) {
        Sensor sensor = SensorBuilder.toEntity(sensorDTO);
        sensor = sensorRepository.save(sensor);
        LOGGER.debug("Sensor with id"+sensor.getId() +" was inserted in db");
        return sensor.getId();
    }

   /* public void delete(UUID sensorId)
    {
        this.sensorRepository.deleteById(sensorId);
    }*/

    public void deleteSensorById(UUID id) {
        sensorRepository.deleteById(id);
        Optional<Sensor> toSave = sensorRepository.findById(id);
        if(toSave.isPresent()){
            LOGGER.debug("Sensor with id {} was not deleted", id);
        }

    }

    public UUID update(UUID id, SensorDetailsDTO sensorDTO)
    {

        Sensor sensor = SensorBuilder.toEntity(sensorDTO);
        sensor.setId(id);
        sensor = sensorRepository.save(sensor);
        LOGGER.debug("Sensor with id" +sensor.getId() + "was inserted in db");
        return sensor.getId();
    }

   /* public UUID update(SensorDetailsDTO sensorDTO){
        Sensor sensor = SensorBuilder.toEntity(sensorDTO);
        sensor.setId(sensorDTO.getId());
        Optional<Sensor> toSave = sensorRepository.findById(sensor.getId());
        if(!toSave.isPresent()){
            LOGGER.debug("Sensor with id {} is not in the db", sensor.getId());
            throw new ResourceNotFoundException(Sensor.class.getSimpleName() + " with id: " + sensor.getId());
        }
        sensorRepository.save(sensor);
        LOGGER.debug("Sensor with id {} was updated", sensor.getId());

        return sensor.getId();
    }*/

    public UUID update(SensorDetailsDTO sensorDTO){
        Sensor sensor = SensorBuilder.toEntity(sensorDTO);
        sensor.setId(sensorDTO.getId());
        Optional<Sensor> toSave = sensorRepository.findById(sensor.getId());
        if(!toSave.isPresent()){
            LOGGER.debug("Person with id {} is not in the db", sensor.getId());
            throw new ResourceNotFoundException(Sensor.class.getSimpleName() + " with id: " + sensor.getId());
        }
        sensorRepository.save(sensor);
        LOGGER.debug("Person with id {} was updated", sensor.getId());

        return sensor.getId();
    }


    public SensorDTO findSensor(SensorDTO sensorDTO)
    {
        List<Sensor> users = sensorRepository.findAll();

        for(Sensor d: users)
        {
            //   if((d.getUsername().equals(personDTO.getUsername())) && (d.getPassword().equals(personDTO.getPassword())))
            // {
            //   return SensorBuilder.toSensorDTO(p);
            // }
        }
        throw new ResourceNotFoundException(" not found!");
    }


}
