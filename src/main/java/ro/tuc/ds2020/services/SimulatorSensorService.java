package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.ClientDTO;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.SimulatorSensorDTO;
import ro.tuc.ds2020.dtos.builders.ClientBuilder;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.dtos.builders.SimulatorSensorBuilder;
import ro.tuc.ds2020.entities.Client;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.SimulatorSensor;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.SensorRepository;
import ro.tuc.ds2020.repositories.SimulatorSensorRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SimulatorSensorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimulatorSensorService.class);
    private final SimulatorSensorRepository simulatorSensorRepository;

    @Autowired
    public SimulatorSensorService(SimulatorSensorRepository simulatorSensorRepository) {
        this.simulatorSensorRepository = simulatorSensorRepository;
    }


    public List<SimulatorSensorDTO> findSimulatorSensor() {
        List<SimulatorSensor> simulatorSensorList = simulatorSensorRepository.findAll();
        List<Double> values=new ArrayList() ;
        for(int i=0;i<simulatorSensorList.size();i++)
        {
            System.out.println(simulatorSensorList.get(i).getMeasurement_value());
            values.add(simulatorSensorList.get(i).getMeasurement_value());
        }
        return simulatorSensorList.stream()
                .map(SimulatorSensorBuilder::toSimulatorSensorDTO)
                .collect(Collectors.toList());


       // for(int i=0;i<)
    }





}
